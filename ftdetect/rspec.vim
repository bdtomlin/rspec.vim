autocmd BufReadPost,BufNewFile *_spec.rb set filetype=rspec.ruby
autocmd FileType rspec setlocal commentstring=#\ %s
